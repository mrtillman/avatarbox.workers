FROM node:latest

WORKDIR /avatarbox.workers

# workaround for dev container
# see https://github.com/zeit/next.js/issues/4022
ENV DEV_ENV=true

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 8802

CMD [ "node", "./Presentation/index.js" ]