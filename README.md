# avatarbox.workers

work queue consumers for [avatarbox.io](https://avatarbox.io)

---

![avatarbox architecture](http://avatarbox.surge.sh/architecture.png)

## Getting Started

**`avatarbox.workers`** is a [Node.js](https://nodejs.org/en/) server that consumes messages published by [avatarbox.web](https://bitbucket.org/mrtillman/avatarbox.web) and [the cron job](https://bitbucket.org/mrtillman/avatarbox.workers/src/master/cron-job.js). This server is responsible for updating the user's Gravatar icon and sending realtime updates via [Pusher Channels](https://pusher.com/channels).

### Prerequisites

|Must have|Nice to have|
|---|---|
|[Gravatar](https://en.gravatar.com/) account|[Docker](https://hub.docker.com)|
|[MongoDB](https://www.mongodb.com) instance|[FluentBit](https://fluentbit.io)|
|[Node.js](https://nodejs.org/en)|[Linux](https://en.wikipedia.org/wiki/Linux_distribution)|
|[Pusher.com](https://pusher.com) account|[Nginx](https://www.nginx.com/)|
|[RabbitMQ](https://www.rabbitmq.com) instance|[Sentry.io](https://sentry.io) account|
|[Redis](https://redis.io) instance|[Timber.io](https://timber.io) account|
|RSA Key Pair||

## Installation

```sh
$ npm install
```

## Tests

```bash
# unit tests
$ npm run test

# end-to-end tests
$ npm run test:e2e

# test coverage
$ npm run test:cov

# acceptance tests
$ npm run test:spec
```

## Usage

```sh
$ npm start
```

## License
[GPL-3.0](https://bitbucket.org/mrtillman/avatarbox.workers/src/master/LICENSE.md)