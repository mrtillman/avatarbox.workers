const UserRepo = require("../Infrastructure/user.repo");

class UserService {
  constructor({ logger }) {
    this.logger = logger;
  }
  get(email) {
    return new Promise((resolve, reject) => {
      UserRepo.findOne({ email }, (err, user) => {
        if (err) return reject(err);
        resolve(user);
      });
    });
  }
  find(query) {
    return new Promise((resolve, reject) => {
      UserRepo.find(query, (err, users) => {
        if (err) return reject(err);
        resolve(users);
      });
    });
  }
  refreshCalendar(email) {
    return new Promise((resolve, reject) => {
      UserRepo.updateOne(
        { email, calendars: { $elemMatch: { name: "Daily" } } },
        { $set: { "calendars.$.lastUpdated": new Date() } },
        (err, status) => {
          if (err) {
            return reject(err);
          } else if (status) {
            const didRefreshCalendar =
              status.ok == 1 && status.n == 1 && status.nModified == 1;
            const didNotModify =
              status.ok == 1 && status.n == 1 && status.nModified == 0;
            const didNotFind =
              status.ok == 1 && status.n == 0 && status.nModified == 0;
            if (didRefreshCalendar) {
              resolve(didRefreshCalendar);
              this.logger.notice("calendar refreshed successfully");
            } else if (didNotModify) {
              this.logger.warn("calendar was not refreshed");
              resolve(didNotModify);
            } else if (didNotFind) {
              this.logger.crit("calendar not found");
              reject(new MissingCalendarError());
            } else {
              const message = `calendar refresh failed | ${status}`;
              this.logger.error(message);
              reject(new Error(message));
            }
          }
        }
      );
    });
  }
}

module.exports = UserService;
