// chmod +x cron-job.js 
// crontab -e
// 0 * * * * /usr/bin/npm --prefix ~/avatarbox.workers/ run cron
// sudo service cron restart

const container = require('./Common/di-container');

const crashReporter = container.resolve("crashReporter");
const logger = container.resolve("logger");
const messageBroker = container.resolve("messageBroker");

logger.useFileTransport("cron-job.log");

messageBroker
.connect().then(() => {
  messageBroker.publish("update.daily", "");
}).then(() => {
  setTimeout(() => {
    logger.notice("daily update succeeded");
    messageBroker.disconnect();
  }, 500);
})
.catch((error) => {
  messageBroker.disconnect();
  logger.error("daily update failed");
  logger.error(error.message);
  crashReporter.submit(error);
  process.exit(1);
});
