require("dotenv").config();
const Sentry = require("@sentry/node");

const isDev = process.env.NODE_ENV != "production";

class CrashReporter {
  constructor() {
    if (isDev) {
      this.reporter = {
        captureException: (error) => {
          console.error(error);
          return "event-id";
        },
      };
    } else {
      Sentry.init({ dsn: process.env.SENTRY_DSN });
      this.reporter = Sentry;
    }
  }
  submit(err) {
    return this.reporter.captureException(err);
  }
}

module.exports = CrashReporter;
