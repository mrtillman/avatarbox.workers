const container = require("../Common/di-container");
const moment = require("moment");

const logger = container.resolve("logger");
const crashReporter = container.resolve("crashReporter");
const messageBroker = container.resolve("messageBroker");
const userService = container.resolve("userService");

function onDailyUpdate() {
  const yesterday = moment().subtract(1, "days");
  userService
    .find({
      calendars: {
        $elemMatch: {
          name: "Daily",
          isEnabled: true,
          lastUpdated: { $lte: yesterday },
        },
      },
    })
    .then((users) => {
      users.map((user) => {
        messageBroker.publish("update.now", user.email);
      });
      return users.length;
    })
    .then((count) => {
      logger.notice(`daily update matched on ${count} records`);
    })
    .catch((error) => {
      logger.warn(error.message);
      if (error.code || error.faultCode) {
        const eventId = crashReporter.submit(error);
        logger.warn(`crash report submitted - event id: ${eventId}`);
      }
    });
}

module.exports = onDailyUpdate;
