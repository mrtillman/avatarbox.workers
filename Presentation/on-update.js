require("dotenv").config();
const moment = require("moment");
const container = require("../Common/di-container");
const { GravatarClient, LoadNextImageUseCase } = require("grav.client");

const logger = container.resolve("logger");
const crashReporter = container.resolve("crashReporter");
const userService = container.resolve("userService");
const rsaService = container.resolve("rsaService");
const cacheService = container.resolve("cacheService");
const pusherClient = container.resolve("pusherClient");

function onUpdate(message) {
  const email = message.content.toString();

  userService
    .get(email)
    .then((user) => {
      // find calendar
      const calendar = user.calendars.find((calendar) => {
        return calendar.name == "Daily";
      });

      // bail out if calendar was
      // updated within the last 24 hours
      const yesterday = moment().subtract(1, "days");
      const lastUpdated = moment(calendar.lastUpdated);
      if (lastUpdated.isAfter(yesterday)) {
        throw new Error("calendar up to date - no refresh was performed");
      }
      return user;
    })
    .then(async (user) => {
      // decrypt password + instantiate gravatar client
      const password = await rsaService.decrypt(user.ciphertext);
      return new GravatarClient(email, password);
    })
    .then((client) => {
      // update gravatar icon
      const useCase = new LoadNextImageUseCase();
      useCase.client = client;
      return useCase.execute().then(() => {
        logger.notice("gravatar icon updated successfully");
        return client;
      });
    })
    .then((client) => {
      userService.refreshCalendar(email);
      return client;
    })
    .then(async (client) => {
      const isOnline = await cacheService.hget(
        client.emailHash,
        "isOnline"
      );
      if (isOnline) {
        pusherClient.channelId = client.emailHash;
        pusherClient.send("Your Gravatar was updated!");
      }
    })
    .catch((error) => {
      logger.warn(error.message);
      if (error.code || error.faultCode) {
        const eventId = crashReporter.submit(error);
        logger.warn(`crash report submitted - event id: ${eventId}`);
      }
    });
}

module.exports = onUpdate;
