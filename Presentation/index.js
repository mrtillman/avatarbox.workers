const container = require("../Common/di-container");
const messageBroker = container.resolve("messageBroker");
const dataStore = container.resolve("dataStore");
const logger = container.resolve("logger");
const crashReporter = container.resolve("crashReporter");
const cacheService = container.resolve("cacheService");

const onUpdate = require("./on-update");
const onDailyUpdate = require("./on-daily-update");

messageBroker
  .onUpdate(onUpdate)
  .onDailyUpdate(onDailyUpdate)
  .connect()
  .then(() => {
    dataStore.connect();
  })
  .then(() => messageBroker.subscribe())
  .catch((error) => {
    messageBroker.disconnect();
    dataStore.disconnect();
    cacheService.disconnect();
    logger.error(error);
    crashReporter.submit(error);
    process.exit(1);
  });
